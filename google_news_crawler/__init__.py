"""Google News Crawler
===================

A utility to fetch news articles from Google News.

GNC retrieves the latest items from the Google News feeds and stores
them in ElasticSearch or on disk.

Written by Isaac Sijaranamual, copyright 2013 University of
Amsterdam/ILPS, licensed under the Apache License, Version 2.0.

"""

__version__ = '0.3.9'
